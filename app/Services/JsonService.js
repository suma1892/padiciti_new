import { Platform, AsyncStorage} from "react-native";
import {RealmQuery} from '../Utils'
import axios from 'axios'
import qs from 'qs'

const JSONPostFile = (action, params) => {
    url  = action
    let   FETCH_TIMEOUT = 180000;
    let formBody = [];
    for (let property in params) {
        let encodedKey = encodeURIComponent(property);
        let encodedValue = encodeURIComponent(params[property]);
        formBody.push(encodedKey + "=" + encodedValue);
    }
    formBody = formBody.join("&");

    return new Promise(((resolve, reject) => {
        var timeout = setTimeout(() => {
            reject(new Error('Request timed out'));
        }, FETCH_TIMEOUT);

     return   fetch(url, {
            method: "POST",
            headers: {

                'Content-Type':   'application/x-www-form-urlencoded; charset=UTF-8',
                'Cache'        : 'no-cache'
            },
          

            body: formBody,
        }).then((response) => {
            clearTimeout(timeout);
            if (response && response.status === 200) return response.json()
            else reject(new Error('Response error'))
        }).then((responseObject) => {
            resolve(responseObject)
        }).catch((err) => {
            reject(err)
        })
    }))
}

const JSONAxioPostFile = (action, params) => {
    
    return axios({
        method: 'post',
        url: action,
        timeout: 180000, // Let's say you want to wait at least 4 mins
        data: qs.stringify(params),
        headers: {
            'Content-Type':   'application/x-www-form-urlencoded; charset=UTF-8',
            'Cache'        : 'no-cache'
        },
        });
}

const JSONGetFile = (action, params) => {
    let   FETCH_TIMEOUT = 200000;
    return new Promise(((resolve, reject) => {
        var timeout = setTimeout(() => {
            reject(new Error('Request timed out'));
        }, FETCH_TIMEOUT);

    return fetch(action, {
        method: "GET",
        headers: {
            'Content-Type': 'text/plain;ISO-8859-18',
        },
    }).then((response) => {
            clearTimeout(timeout);
            if (response && response.status === 200) {
                return response.json()}
            else reject(new Error('Response error'))
        }).then((responseObject) => {
            resolve(responseObject)
        }).catch((err) => {
            reject(err)
        })
    }))
    
}

const JSONGetFilexxxx = (action, params) => {
    let   FETCH_TIMEOUT = 180000;
    
    return new Promise(((resolve, reject) => {
        var timeout = setTimeout(() => {
            reject(new Error('Request timed out'));
        }, FETCH_TIMEOUT);

     return fetch(action, {
        method: "GET",
        headers: {
            'Content-Type': 'text/plain;charset=UTF-8',
        },
     }).then((response) => {
            clearTimeout(timeout);
            if (response && response.status === 200) {
                return response.json()
            }
            else reject(new Error('Response error'))
        }).then((responseObject) => {
            resolve(responseObject)
        }).catch((err) => {
            reject(err)
        })
    }))
    
}


const JSONGetFile1 = (action, train_depart, pax_list, client_email) => {
    // constructor() {
        this.state = {
            dataSource         : null,
            client_email        : null,
            client_phone        : null
            
        }
    // }

    let listBooking=[]
    for (let x = 0;x< train_depart.lst.length;x++){
        let tamp = {eventId:train_depart.InvEvId,scheduleId:train_depart.lst[x].ScheduleId}
        listBooking.push(tamp)
    }
    let trnType = train_depart.lst.length > 1 ? 'multiple-book' : 'single-book'

    let JsonBook = {      
            detailBooks: listBooking, 
            email: client_email,
            numPaxAdult:pax_list.Adult,
            numPaxChild:pax_list.Child,
            numPaxInfant:pax_list.Infant,
            trnType : trnType
        }

    return fetch(action, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },

        body: JSON.stringify({
            
            detailBooks: listBooking, 
            email: client_email,
            numPaxAdult:pax_list.Adult,
            numPaxChild:pax_list.Child,
            numPaxInfant:pax_list.Infant,
            trnType : trnType
        })
  })
  .then((response) => response.text())
  .then((responseJson) => {
    return responseJson
    this.setState({
    //   isLoading: false,
      dataSource: responseJson.Data,
    }, function(){

    });

  })
  .catch((error) =>{
  });
}


const JSONPost_ = (url, params) => {
    const options = {
        method  : 'POST',
        timeout : 200000,
        headers : { 'content-type': 'application/x-www-form-urlencoded' },
        data    : qs.stringify(params),
        url,
    };
    return axios(options)
}

const JSONGet_ = (url, params) => {

    const options = {
        method  : 'GET',
        timeout : 200000,
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
        },
        data    : qs.stringify(params),
        url,
    };

    return axios(options)

}

const JSONGetV2_ = async (url,params) =>{
    const data = await axios.get(url,  {
        timeout: 200000
      })
      return data
}
export { JSONGetFile1, JSONPostFile, JSONGetFilexxxx,JSONGetFile, JSONPost_, JSONGet_, JSONAxioPostFile } 

