import React, { Component } from 'react'
import {
    StyleSheet,
    ImageBackground,
    View,
    FlatList,
    ScrollView,
    Linking,
    BackHandler,
     Animated, TouchableOpacity,
    Image, Text, Dimensions, AppRegistry, AsyncStorage,Alert
} from 'react-native'
import { Function, FunctionGetWindow, navigateTo } from '../Utils'
import { TextView, Colors, Metrics, Container, getIcon, Toolbar, CardComponent, RadioButtons, Touchable, CardComponentFilter, Modal, Button, ToolbarHome } from '../Components'
import Navbar from '../Components/NavigationBottom'
import ArrayMenu from '../Utils/array';
import Slick from 'react-native-slick';
import { TabNavigator, StackNavigator, NavigationActions } from 'react-navigation';
import { Scale, Fonts } from '../Assets/index';
import s from '../Components/Styles';
import { STRING } from '../Utils'
// import { ListProperty } from '../Utils/dummy'
const viewabilityConfig = {
    minimumViewTime: 10,
    viewAreaCoveragePercentThreshold: 100,
    waitForInteraction: true,
};
import moment from 'moment'
import { API, getURL } from '../Services/API'
import { Parameter } from '../Services/Parameter'
import { JSONPostFile } from '../Services/JsonService'
let {width, height} = Dimensions.get('window');

export default class Home extends Component {
    constructor(props) {
        super(props);
        this._onScroll = this._onScroll.bind(this)
        this.springValue = new Animated.Value(100)
        this.state = {
            backClickCount: 0,
            product: null,
            scroll: true,
            parameterbaner: {
                app: 'newhome'
            },
            parameterTour: {
                app: 'tourpackage'
            },
            parameterpoint: {
                email: null,
                resellerCode: 'padiciti',
                resellerPass: 'padiciti123'
            },
            tourpackage: [],
            point: 0,
            padicash: 0,
            name: null,
            Banner: [
                {banner:getIcon('banner4')},
                {banner:getIcon('banner1')},
                {banner:getIcon('banner2')},
                {banner:getIcon('banner3')},
            ],
            titleTime: null

        }
    }
    _shouldItemUpdate = (prev, next) => {
        return prev.item !== next.item;
    }

    componentWillMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
    }

    _spring() {
        this.setState({backClickCount: 1}, () => {
            Animated.sequence([
                Animated.spring(
                    this.springValue,
                    {
                        toValue: -.15 * height,
                        friction: 5,
                        duration: 300,
                        useNativeDriver: true,
                    }
                ),
                Animated.timing(
                    this.springValue,
                    {
                        toValue: 100,
                        duration: 300,
                        useNativeDriver: true,
                    }
                ),

            ]).start(() => {
                this.setState({backClickCount: 0});
            });
        });

    }


    handleBackButton = () => {
        this.state.backClickCount == 1 ? BackHandler.exitApp() : this._spring();

        return true;
    };

    componentDidMount() {

        this.setState({ titleTime: Function.TimeTitle() })
        // AsyncStorage.getItem('Banner', (err, Banner) => {
        //     if (Banner) {
        //         if (Function.JsonParse(Banner).time === Function.getTime()) {
        //             this.Auth('url_list_headlines')
        //         } else {
        //             this.setState({ Banner: Function.JsonParse(Banner).Banner }, () => {

        //             })
        //         }
        //     } else {
        //         this.Auth('url_list_headlines')
        //     }
        // })

        AsyncStorage.getItem('UserData', (err, UserData) => {
            if (UserData) {
                this.setState({ name: Function.JsonParse(UserData).client_name, }, () => {
                    this.state.parameterpoint.email = Function.JsonParse(UserData).client_email
                    this.Auth('url_point')
                })

            } else {
                this.Auth('url_list_headlines', 'tour')
            }
        })
    }

    _onScroll(e) {
        this.setState({
            scroll: Function.onScroll(e)
        });
    }

    Profile() {
        AsyncStorage.getItem('UserData', (err, UserData) => {
            if (UserData) {
                this.props.navigation.navigate('LandingPageProfile')
            } else {
                this.props.navigation.navigate('Login', { slug: 'profile' })
            }
        })
    }

    ClickMenu(value) {
        let { navigate } = this.props.navigation
        switch (value) {
            case 'flight':
                this.props.navigation.navigate('FlightScreen')
                break
            case 'train':
                this.props.navigation.navigate('TrainReservation')
                break
            case 'hotel':
                this.props.navigation.navigate('LandingPageHotel')
                break
            case 'bus':
                this.props.navigation.navigate('BusReservation')
                break
            case 'flight_int':
                this.props.navigation.navigate('FlightScreenINT')
                break
            case 'Padiplay':
                Linking.openURL('https://play.google.com/store/apps/details?id=com.padiplay')
                break
            case 'myTrip':
                this.props.navigation.navigate('HistoryBooking')
                break
            case 'tours':
                this.props.navigation.navigate('Tour')
                break
            case 'pln':
                this.props.navigation.navigate('PlnReservation')
                break
            case 'more':
                this.setState({ visibleModal: true })
                break
            case 'pulsa':
                // this.props.navigation.navigate('HomePulsa')
                // Alert.alert("PULSA")
                break
        }
    }

    Auth(type, tour) {
        try {
            this.setState({ loading: true })
            let url = getURL(type)
            let Parameter = type === 'url_list_headlines' ? tour ? this.state.parameterTour : this.state.parameterbaner : this.state.parameterpoint

            JSONPostFile(url, Parameter).then((Respone) => {

                switch (Respone.resp_code ? Respone.resp_code : Respone.respCode) {

                    case '0':
                        switch (type) {
                            case 'url_point':
                                this.setState({ point: Respone.saldoPoint }, () => {
                                    this.Auth('url_list_headlines', 'tour')
                                })
                                break
                        }
                        break
                    default:
                        this.setState({ loading: false }, () => {

                        })
                        break
                }

            }).catch((err) => {
                this.setState({ loading: false }, () => {
                })

            })
        } catch (Error) {
            this.setState({ loading: false }, () => {
            })
        }

    }


    render() {
        let { navigation } = this.props
        return (
            <View style={{ flex: 1, backgroundColor: '#EAEDF7', }} >
            <Animated.View style={[styles.animatedView, {transform: [{translateY: this.springValue}]}]}>
                    <Text style={styles.exitTitleText}>press back again to exit the app</Text>
                    <TouchableOpacity
                        activeOpacity={0.9}
                        onPress={() => BackHandler.exitApp()}
                    >
                    </TouchableOpacity>
                </Animated.View>
                <View>
                <ToolbarHome>
                <View style={{flex: 1,height:'100%', width:'100%', backgroundColor: '#EAEDF7'}}>
                <ImageBackground source={getIcon('world')} style={{width: '100%', height: '92%'}}>
                    <View style={{flex: 1,
                        flexDirection: 'row',
                        paddingLeft: Metrics.normalPadding,
                        paddingRight: Metrics.normalPadding,
                        alignItems: 'center',
                        justifyContent: 'space-between',}}>
                        <View style={{ padding: Scale(8) }}>
                        <TextView style={styles.GoodEveningStyle}>{this.state.titleTime}</TextView>
                        <TextView style={styles.NameStyle}>{this.state.name}</TextView>
                    </View>
                    <View style={{ flex: 1, alignItems: 'center', }}>
                    </View>
                    <View>
                        <Touchable
                            onPress={() => this.Profile()}>
                            <Image
                                style={{ width: Scale(40), height: Scale(40) }}
                                resizeMethod='scale'
                                source={getIcon('ic_shape')}
                            />
                        </Touchable>
                    </View>
                    </View>

                            <View style={{flexDirection: 'row',
                            padding:10,
                            position:'absolute',
                            left:     20,
                            top:      110,
                            width:Scale(150),
                            borderRadius:5,
                            alignItems: 'center',
                            height: Scale(50),
                            backgroundColor: "white"}}>

                                <Image
                                    style={[styles.ic_starYellow]}
                                    resizeMethod='scale'
                                    source={getIcon('ic_star_yellow')}
                                />
                                <Text >PadiPoint {this.state.point}</Text>
                            </View>


                            {/*<View style={styles.line4}>

                    </View>*/}

                            <Touchable
                            onPress={() => this.props.navigation.navigate('SelectTour', { data: {title : 'PadiPay Deposit', page :'https://www.padipay.com/deposit/'} })}>
                            <View style={{ flexDirection: 'row',
                            padding:10,
                            position:'absolute',
                            right:     20,
                            top:      110,
                            width:Scale(150),
                            borderRadius:5,
                            alignItems: 'center',
                            height: Scale(50),
                            backgroundColor: "white" }}>
                                <Image
                                    style={[styles.ic_cash]}
                                    resizeMethod='scale'
                                    source={getIcon('ic_cash')}
                                />

                                <Text style={{alignItems: 'center',left:20,alignSelf:'center',justifyContent:'center',}}>Deposit {' Rp ' + this.state.padicash}</Text>
                            </View>
                            </Touchable>
                    </ImageBackground>
                    </View>
                </ToolbarHome>

                <Modal
                    type={'more'}
                    active={this.state.visibleModal}
                    onClose={value => this.setState({ visibleModal: value })}
                >
                    <View style={styles.modal}>
                        <View style={styles.header}>
                            <View style={{ flex: 1 }}>
                                <Text style={styles.moreProduct}>More Product</Text>
                            </View>
                            <View style={s.center}>
                                <Touchable onPress={console.log('')}>
                                    <View style={s.close}>
                                        <Text style={s.itemClose}>Edit Favorite</Text>
                                    </View>
                                </Touchable>
                            </View>
                        </View>
                        <View style={styles.contentModal}>
                            <FlatList
                                scrollEnabled={false}
                                data={ArrayMenu.ArrayMoreProduct()}
                                renderItem={({ item }) => (
                                    <View style={{
                                        marginBottom: Metrics.padding.small,
                                        marginLeft: Scale(8)
                                    }}>
                                        <CardComponentFilter
                                            label={item.title}
                                            icons={item.source}
                                            onChoose={() => this.setState({ visibleModal: false }, () => { this.ClickMenu(item.slug) })}
                                        />
                                    </View>
                                )}
                                keyExtractor={(item, index) => index}
                                numColumns={4} />

                        </View>
                        
                    </View>
                </Modal>
                
                        </View>
                <ScrollView ref={'scrollView'}
                    style={{ marginBottom: Metrics.navBot }}
                    onScroll={this._onScroll}>
                {//image banner
                }

                    <View style={{ marginTop: Scale(4) }}>
                        
                        <FlatList
                            contentContainerStyle={{ marginHorizontal: Scale(4) }}
                            scrollEnabled={false}
                            data={ArrayMenu.ArrayDashBoard()}

                            renderItem={({ item }) => (
                                <View style={{ flex: 1, justifyContent: 'center',}}>
                                    <CardComponent
                                        style={{width: 40, height: 40, color:'red',backgroundColor:'red'}}
                                        label={item.title}
                                        icons={item.source}
                                        ic_bg ={(item.id === '7') ? 'ic_blank' :'ic_bg_active'}
                                        onPress={() => this.ClickMenu(item.slug)}
                                    />
                                </View>
                            )}
                            keyExtractor={(item, index) => index}
                            numColumns={4} />

                    </View>
                    <View
                    style={{
                        borderWidth: 0.5,
        borderColor:'grey',
        margin:10,
                    }}
                    />

                    {<View
                        style={{ height: Scale(170), marginTop: Scale(10), padding: Scale(16), backgroundColor: 'white' }}>

                        <TextView style={styles.selectYourFavorite}>{'Promo'}</TextView>


                        <FlatList
                            horizontal
                            data={this.state.Banner}
                            keyExtractor={(item, index) => `key-${index}`}
                            renderItem={({ item }) => (
                                <Touchable
                                    onPress={() => navigateTo('Promos', this.props.dispatch, this.props.navigation, null)}>
                                    <View style={[styles.rectangle4]}>
                                        <Image
                                            style={[styles.bitmap]}
                                            resizeMethod='scale'
                                            source={item.banner}
                                        />
                                    </View>
                                </Touchable>
                            )} />
                            </View> }
                </ScrollView>

                {/* {this.state.scroll && <View style={{
                    left: 0,
                    right: 0,
                    bottom: Scale(65),
                    position: 'absolute',
                    justifyContent: 'center',
                    alignItems: 'center'
                }}>

                    <View style={{ flex: 1, marginBottom: Scale(4) }}>
                        <Image
                            style={[styles.img_scroll_more]}
                            resizeMethod='scale'
                            source={getIcon('ic_scroll_more')}
                        />
                    </View>
                </View>} */}
                <Navbar active='home'
                    navigation={navigation} />





            </View>
        );
    }
}

const styles = StyleSheet.create({
    animatedView: {
        width,
        backgroundColor: "red",
        elevation: 2,
        position: "absolute",
        bottom: 0,
        padding: 10,
        justifyContent: "center",
        alignItems: "center",
        flexDirection: "row",
    },
    exitTitleText: {
        textAlign: "center",
        color: "#ffffff",
        marginRight: 10,
    },
    exitText: {
        color: "#8A8988",
        paddingHorizontal: 10,
        paddingVertical: 3
    },
    contentModal: {
        paddingVertical: Metrics.padding.medium
    },
    save: {
        marginTop: Metrics.padding.tiny
    },
    modal: {
        paddingTop: Metrics.padding.normal,
    },
    moreProduct: {
        fontFamily: Fonts.bold.fontFamily,
        fontWeight: Fonts.bold.fontWeight,
        fontSize: Metrics.font.medium,

    },
    header: {
        marginHorizontal: Metrics.padding.medium,
        marginVertical: Metrics.padding.small,
        flexDirection: 'row',
    },
    rectangle9: {
        flexDirection: 'row',
        padding:10,
        position:'absolute',
        left:     0,
        top:      110,
        width:Scale(350),
        borderRadius:5,
        alignItems: 'center',
        height: Scale(39),
        backgroundColor: "grey"
    },
    banner: {
        width: Metrics.screenWidth,
        height: Scale(170)
    },
    rectangle6: {
        justifyContent: 'center',
        right: Scale(8),
        width: Scale(80),
        height: Scale(22),
        marginBottom: Scale(16),
        borderRadius: Scale(11),
        backgroundColor: Colors.tangerine
    }, allPromos: {
        flex: 1,
        textAlign: 'center',
        fontSize: Metrics.font.tiny,
        color: "#ffffff"
    },
    line4: {
        width: Scale(2),
        height: Scale(22),
        borderStyle: "solid",
        borderWidth: 1,
        borderColor: "#dddddd"
    }, ic_cash: {
        left: Scale(15),
        width: Scale(14),
        height: Scale(14)
    },
    ic_starYellow: {
        right: Scale(4),
        width: Scale(14),
        height: Scale(14)
    },
    NameStyle: {
        fontSize: Scale(16),
        fontStyle: "normal",
        letterSpacing: 0,
        textAlign: "left",
        color: 'white'
    },
    GoodEveningStyle: {
        fontSize: Scale(14),
        fontStyle: "normal",
        textAlign: "left",
        color: 'white'
    },
    selectYourFavorite: {
        height: Scale(19),
        marginBottom: Scale(6),
        fontSize: Scale(14),
        fontWeight: "500",
        fontStyle: "normal",
        letterSpacing: 0,
        textAlign: "left",
        color: "#000000"
    },
    bitmap: {
        width: Scale(240),
        height: Scale(120),
        borderRadius:5

    },
    rectangle4: {
        width: Scale(250),
        height: Scale(300),
        paddingLeft:Scale(10),
        paddingRight:Scale(10),
        marginRight: Scale(10),
        borderRadius: Scale(6),
        backgroundColor: "#ffffff"
    },
    label_tour: {
        height: Scale(18),
        fontSize: Scale(13),
        fontWeight: "500",
        fontStyle: "normal",
        letterSpacing: 0,
        textAlign: "center",
        marginTop: Scale(4),
        color: "#606060"
    },
    img_scroll_more: {
        width: Scale(70),
        height: Scale(20)
    }, more: {
        flex: 1

    }, slide1: {
        flex: 1,
        backgroundColor: '#9DD6EB',
    },

});

AppRegistry.registerComponent("padiciti", () => Home);
