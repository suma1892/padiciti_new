import React, { Component } from 'react'
import {
    AppRegistry,
    TouchableOpacity,
    ImageBackground,
    View,
    Image,
    StyleSheet,
    Alert,
    ScrollView,
    Animated,BackHandler,
    AsyncStorage
} from 'react-native'
import moment from 'moment'
import {
    TextView as Text,
    ToolbarTrain,
    Icon,
    TrainRadio,
    TrainButton,
    TrainFormDate,
    Touchable,
    TrainStepper,
    TrainForm,
    CardRecentSearch,
    ToolbarHome
} from '../../Components/'
import { Colors, Scale, getIcon, Metrics, _OS} from '../../Assets'
import { STRING_TR, Function, STRING, navigateTo } from '../../Utils';
import { AnimatedValue, toCapitalize } from '../../Utils/TrainUtils';
import RadioGroup from 'react-native-custom-radio-group';

export default class TrainReservation extends Component {
    constructor(props) {
        super(props);
        this.state = {
            origination : ['GMR', 'Gambir'],
            destination : ['BD', 'Bandung'],
            pax_adult   : 1,
            pax_infant  : 0,
            type_trip   : 'oneway',
            depart_date : this.getDate('depart_date'),
            return_date : this.getDate('return_date'),
            animated    : new Animated.Value(0),
            timeIsComing:moment(new Date()).format('YYYY-MM-DD')
        }
    }

    getDate =(slug, return_date)=> {
        
        if (slug==='depart_date') return moment(new Date()).format('YYYY-MM-DD')

        if(slug==='return_date') return moment(return_date).add(3, 'day').format('YYYY-MM-DD')
        }
    

    setPax = (target, value) => {
        this.setState({[target]: value}, () => {
            if (this.state.pax_infant > this.state.pax_adult)
                this.setPax('pax_infant', this.state.pax_adult)
        })
    }

    ActivityResult = (data) => {
        let { navigation } = this.props
        switch (data.slug) {
            case 'origination': 
                return this.setState({origination: data.stationList})
            case 'destination': 
                return this.setState({destination: data.stationList})
            case 'depart_date':
                let {
                type_trip
            } = this.state

                this.setState({ depart_date: data.depart_date, return_date: moment(data.depart_date).add(2, 'day').format('YYYY-MM-DD') }, () => {
                    type_trip !== 'oneway' && this.props.navigation.navigate('TrainCalendar', { slug: 'return_date', type_trip, depart_date: data.depart_date, return_date: moment(data.depart_date).add(2, 'day').format('YYYY-MM-DD'), ActivityResult: this.ActivityResult })
                })
                break
            case 'return_date': 
                return this.setState({return_date: data.return_date }, () => {
                let range = moment(this.state.return_date, 'YYYY-MM-DD').diff(moment(this.state.depart_date, 'YYYY-MM-DD'), 'days')
                    range < 1 && this.setState({return_date: data.depart_date})
                })
        }
    }


    toggle_animate = () => {
        Animated.timing(this.state.animated,{ toValue: this.state.type_trip === 'oneway' ? 0 : 1,duration: 500 }).start()
    }

    reserveAction = () => {
        let pax_list = {
            type_trip   : this.state.type_trip,
            origination : this.state.origination,
            destination : this.state.destination,
            depart_date : this.state.depart_date,
            return_date : this.state.return_date,
            pax_adult   : this.state.pax_adult,
            pax_infant  : this.state.pax_infant,
        }

        if (this.state.type_trip === 'roundtrip') pax_list = {...pax_list, return_date: this.state.return_date }
        Function.SaveDataJson('SearchTrain1', Function.JsonString(pax_list))
        this.props.navigation.navigate('TrainScheduleDepart', { pax_list } )
    }
    componentWillMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
    }
    handleBackButton = () => {
        this.props.navigation.navigate('HomeScreen')
        return true;
    };
    componentDidMount() {
        this.getDate('depart_date')
        this.getDate('return_date')
        AsyncStorage.getItem('SearchTrain1', (err, SearchTrain1) => {
            if (SearchTrain1 !== null) {
                let history_search = Function.JsonParse(SearchTrain1)

                range_today = moment(history_search.depart_date, 'YYYY-MM-DD').diff(moment(new Date()).format('YYYY-MM-DD'), 'days')

            // Change the depart_date if it's past from today
            history_search.depart_date = range_today >= 0 ? history_search.depart_date : moment(new Date()).format('YYYY-MM-DD')

            let range       = moment(history_search.return_date, 'YYYY-MM-DD').diff(moment(this.state.depart_date, 'YYYY-MM-DD'), 'days')
                
            this.setState({
                    origination : history_search.origination,
                    destination : history_search.destination,
                    pax_adult   : history_search.pax_adult,
                    pax_infant  : history_search.pax_infant,
                    type_trip   : "oneway",
                    depart_date : history_search.depart_date,
                    return_date : range < 1 ? moment(history_search.depart_date, 'YYYY-MM-DD').add('day', 1) : history_search.return_date,
                }, () => {
                    this.state.type_trip === 'oneway' && AnimatedValue(this.state.animated, 0)
                })
            }
        })
    }
    


    render() {
        let { ActivityResult } = this
        let { navigation } = this.props
        let {
            origination,
            destination,
            depart_date,
            pax_adult,
            pax_infant,
            type_trip,
            return_date
        } = this.state

        // this.setState({return_date : this.getDate('return_date')}) 
        const toggle_return     = this.state.animated.interpolate({inputRange: [0, 1], outputRange: [0, Metrics.screenHeight] })
        const toggle_return2    = this.state.animated.interpolate({inputRange: [0, 1], outputRange: [0, 1] })
        // const toggle_return = AnimatedInter(this.state.animated, [0,1], [0,undefined])

        return (
            <View style={s.container} >
                
                <ToolbarTrain>
                <View style={{flex: 1,height:'100%', width:'100%', backgroundColor:'#EAEDF7'}}>
                    <ImageBackground source={getIcon('world')} style={{width: '100%', backgroundColor:'#EAEDF7', height: '92%'}}>
                    <View style={{flexDirection: 'row',}}>
                    <Touchable
                        onPress={() => this.props.navigation.goBack(null)}>
                        <Image
                            style={{ width: Scale(40), height: Scale(40) }}
                            resizeMethod='scale'
                            source={getIcon('ic_arrow_right_')}
                        />
                        
                    </Touchable>
                    <Text style={{color:'white', fontSize:Scale(25)}}>Search Train</Text>
                </View>
                <View style={{padding:Scale(10), paddingTop:Scale(1), top:Scale(5)}}>
                <TrainForm
                    type        = {'option'}
                    label       = {'Origin City'}
                    value       = {(origination && origination[0].toUpperCase()) || null}
                    city       = {(origination && toCapitalize(origination[1])) || null}
                    placeholder = {STRING.Label.org}
                    icon_left   = {'ic_train'}
                    onPress     = {() => { 
                        navigation.navigate('TrainStationList', { slug: 'origination', ActivityResult: this.ActivityResult })
                        }} />
                </View>

                <Icon size={'normal'} source={getIcon('ic_switch')} style={s.icon_switch} onPress={() => {
                    [origination, destination] = [destination, origination]
                    this.setState({origination, destination})
                }}/>

                <View style={{paddingTop:Scale(-30),padding:Scale(10)}}>
                <TrainForm
                    type        = {'option'}
                    label       = {'Destination City'}
                    value       = {(destination && destination[0].toUpperCase()) || null}
                    city        = {(destination && toCapitalize(destination[1])) || null}
                    placeholder = {STRING_TR.LABEL.ORG_PH}
                    icon_left   = {'ic_train'}
                    onPress     = {() => navigation.navigate('TrainStationList', { slug: 'destination', ActivityResult: this.ActivityResult })} />
                    
                </View>
                <View style ={{borderRadius:40,padding:10,
                position:'absolute',
                left:     Scale(27),
                top:      Scale(250),
                alignItems: 'center',
                backgroundColor: "white"}}>
                <Option
                    options={['oneway', 'roundtrip']}
                    onChange={(option) => {
                        this.setState({ type_trip: option, return_date:this.getDate('return_date', depart_date) }, () => 
                        AnimatedValue(this.state.animated, this.state.type_trip==="oneway"?0:1))
                    }}
                />
                </View>
                </ImageBackground>
            </View>
                </ToolbarTrain>

                <ScrollView  bounces={false} style={{ flex: 1 }}>
                    <View style={{ marginLeft: Scale(10), marginRight: Scale(10) }}>

                        {/*  DATE SECTION */}
                        <TrainFormDate
                            type        = {'option'}
                            label       = {'Berangkat'}
                            value       = {moment(depart_date).format('DD MMM YYYY')}
                            icon_left   = {'ic_calender_go'}
                            onPress     = {() => navigation.navigate('TrainCalendar', { slug: 'depart_date', type_trip, depart_date, ActivityResult: this.ActivityResult })} />
                        
                            {this.state.type_trip==="oneway"?false:true&&<TrainFormDate
                                type        = {'option'}
                                label       = {'Pulang'}
                                value       = {moment(return_date).format('DD MMM YYYY')}
                                icon_left   = {'ic_calender_back'}
                                onPress     = {() => navigation.navigate('TrainCalendar', { slug: 'return_date', type_trip, depart_date, return_date, ActivityResult: this.ActivityResult })} />}
                        

                        {/* PAX SECTION */}
                        <View style={{ flexDirection: 'row' }}>
                            <TrainStepper
                                label   = {STRING_TR.LABEL.PASSENGER.ADULT}
                                min     = {1}
                                max     = {4}
                                onPress = {value =>  this.setPax('pax_adult', value)}
                                value   = {pax_adult}
                                style   = {{ flex:1, marginRight: Scale(8)}}
                            />

                            <TrainStepper
                                label       = {STRING_TR.LABEL.PASSENGER.INFANT}
                                extra_label = {'(< 3 thn)'}
                                min         = {0}
                                max         = {pax_adult}
                                onPress     = {value => this.setPax('pax_infant', value)}
                                value       = {pax_infant}
                                style       = {{ flex:1, marginRight: Scale(8)}}
                            />
                            <View style={{flex:1}}/> 
                        </View>
                    </View>
                    <TrainButton
                        onPress={() => this.reserveAction()} style={s.btn_reserve}>{STRING_TR.LABEL.BTN_SEARCH}</TrainButton>

                    {/* <View style={{ padding: Scale(16) }}>
                        <Text style={{ color: Colors.black }}>{STRING.Label.recent_Search}</Text>
                        <CardRecentSearch
                            onPress={() => console.log('')}
                            destination={'Bandung (BD) - Gambir (GMR)'}
                            date={'13 November 2018'}
                            guest={'1 Dewasa, 1 Bayi'}
                        />
                    </View> */}
                </ScrollView>
            </View>
        )
    }
}

class Option extends Component {
    constructor(props) {
      super(props);
      this.state = {
        activeOption: this.props.options[0],
      };
    }
    updateActiveOption = (activeOption) => {
      this.setState({
        activeOption,
      });
    };
    render() {
      return (
        <View
          style={{
            justifyContent: 'center',
            flexDirection: 'row',
            flexWrap: 'wrap',
          }}
        >
          {this.props.options.map((option, index) => (
            <TouchableOpacity
              onPress={() => {
                this.props.onChange(option);
                this.updateActiveOption(option);
              }}
            >
              <Text
                style={{
                  width: Scale(140),
                  height: Scale(40),
                  paddingLeft:Scale(45),
                  paddingTop:Scale(12),
                  borderRadius:20,
                  backgroundColor:this.state.activeOption === option ? '#385FDC' : 'white',
                  color: this.state.activeOption === option ? 'white' : 'grey',
                }}
              >
                {option}
              </Text>
            </TouchableOpacity>
          ))}
        </View>
      );
    }
  }

const s = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#EAEDF7'
    },
    toolbar_title: {
        fontSize: Scale(18),
        color: 'white',
    },

    icon_switch: {
        position: 'absolute',
        alignSelf: 'center',
        alignItems: 'center',
        top: Scale(134),
        left: Scale(10)
    },
});

AppRegistry.registerComponent("padiciti", () => TrainReservation);
