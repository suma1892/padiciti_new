import React, { Component } from 'react'
import {
    View,
    StyleSheet,
    TouchableWithoutFeedback,
    TouchableNativeFeedback, Image,
    Dimensions, 
  Platform,
  PixelRatio
} from 'react-native'
import {
    Scale,
    Colors,
    getIcon,
    Metrics,
    Fonts,
} from '../Assets'
import { Function } from '../Utils'
import { Icon } from './IconComponent'
import Text from './TextView'
import { _OS } from './_OS';
import QRCode from 'react-native-qrcode'
import Dash from 'react-native-dash';

const { width, height } = Dimensions.get('screen');
const InlineImage = (props) => {
    let style = props.style;
    if (style && Platform.OS !== 'ios') {
      // Multiply width and height by pixel ratio to fix React Native bug
      style = Object.assign({}, StyleSheet.flatten(props.style));
      ['width', 'height'].forEach((propName) => {
        if (style[propName]) {
          style[propName] *= PixelRatio.get();
        }
      });
    }
  
    return (
      <Image
        {...props}
        style={style}
      />
    );
  };
export class CardTrain extends Component {
    constructor(props) {
        super(props);
        this.state = {
        }
    }


    render() {
        let {
            name,
            seats,
            price,
            train_class,
            depart_time,
            arrive_time,
            train_no,
            orgCode,
            desCode,
            duration,
            stopover,
            onPress,
            onPressMore
        } = this.props
        let Touchable = _OS(TouchableWithoutFeedback, TouchableNativeFeedback)
        let ava = ""
                        if (seats >= 50 ){
                            ava = ""
                        }else{
                            ava = seats.toString()+" Kursi"
                        }
        return (
            <View style={{borderRadius:20, backgroundColor:'white', shadowOffset:{  width: 10,  height: 10,  },
            shadowColor: 'black',
            shadowOpacity: 1.0,}}>

            <Touchable onPress={()=> seats !== 0 && onPress()}>
            <View style={[s.card_frame, seats <= 0 && {borderRadius: 20, }]}>
                    <View style={{borderRadius:20}}>
                    <View style={{justifyContent: 'space-between', flexDirection: 'row',paddingLeft:Scale(10),paddingTop:Scale(10), paddingRight:Scale(10)}}>
                            <View>
                                <Text style={{fontWeight: 'bold'}}>{name} - {train_no}   <Text style={{color:seats<=0?'red':'green'}}>{ava}</Text></Text>
                            </View>
                    </View>
                    <View style={{justifyContent: 'space-between', flexDirection: 'row',paddingLeft:Scale(10),paddingTop:Scale(3), paddingRight:Scale(10)}}>
                        <View>
                            <Text style={{fontWeight: 'bold'}}>{train_class}</Text>
                        </View>
                        <View style={{color: 'red'}}>
                            <Text style={{color: 'orange', fontSize:Fonts.size.regular, fontWeight:'bold'}}>RP { Function.convertToPrice(price)}</Text>
                        </View>
                    </View>

                        <View>
                            <Dash style={{width:Scale(310), height:1, flexDirection:'row'}}/>
                        </View>
                        <View style={{
                            flex: 1,
                            paddingTop:5,
                            padding:10,
                            flexDirection: 'row',
                            justifyContent: 'space-between',
                        }}>
                            <View><Text style={{fontWeight: 'bold', fontSize:Fonts.size.regular}}>{orgCode} - {desCode}</Text></View>
                            <View><Text style={{color: 'grey'}}>Duration</Text></View>
                            <View><Text style={{color: 'grey', fontSize:Fonts.size.small}}>{stopover}</Text></View>
                        </View>
                        <View style={{
                            flex: 1,
                            padding:Scale(3),
                            flexDirection: 'row',
                            justifyContent: 'space-between',
                        }}>
                            <View><Text style={{fontWeight:'bold', fontSize:Fonts.size.regular}}>{depart_time}  -  {arrive_time}</Text></View>
                            <View><Text style={{fontWeight:'bold', color:'grey', fontSize:Fonts.size.regular}}>{duration}</Text></View>
                            <View><Text>          </Text></View>
                        </View>
                        <View style={{
                            padding:Scale(3),
                            flexDirection: 'row',
                            justifyContent: 'space-between',
                            alignItems:'center'

                        }}>
                            <Text style={{color:'green', fontSize:Fonts.size.small}}><InlineImage
                                style={{height:Scale(7),width:Scale(7)}}
                                source={getIcon('ic_aca_asuransi')}
                                /> Full Refund
                            </Text>

                            <Text style={{fontWeight:'bold', color:'green',fontSize:Fonts.size.small}}>
                            <InlineImage style={{height:Scale(7),width:Scale(7)}} source={getIcon('ic_delay_protect')}
                            /> padiciti Protection</Text>
                            <Touchable onPress={onPressMore}>
                            <Text style={{fontWeight:'bold', color:'orange',fontSize:Fonts.size.small}}>
                            <InlineImage style={{height:Scale(8),width:Scale(8)}} source={getIcon('')}
                            /> Lihat Detail &#10140;</Text>
                        
                </Touchable>
                    </View>
                    </View>
                    
                </View>
            </Touchable>
            </View>

        )
    }
}




export class CardShortPax extends Component {
    constructor(props) {
        super(props)
        this.state = {}
    }

    render() {
        let { props } = this
        let Touchable = _OS(TouchableWithoutFeedback, TouchableNativeFeedback)

        return (
            <View style={[s.card_pax_frame]}>
                {props.index && <Text style={s.name}>{props.index}.</Text>}
                <View style={s.card_pax_content}>
                    {props.name     && <Text style={s.name}>{props.name}</Text>}
                    {props.num_id && <Text style={[s.sub_text, { ellipsizeMode: 'tail', color: '#888888' }]}
                        ellipsizeMode={'tail'}
                        numberOfLines={1}
                    >{props.num_id}</Text>}

                    <View style = {{flexDirection : 'row'}}>

                    <View style={{width : this.props.widthDest ? this.props.widthDest:Metrics.screenWidth/3}}>
                    <Text style={[s.sub_text, {ellipsizeMode : 'tail', color : '#888888'}]}
                    ellipsizeMode = {'tail'}
                    numberOfLines = {1}
                    >{props.org}</Text>

                    </View>
                    {props.seat && <View style={{width : this.props.widthSeat ? this.props.widthSeat  :Metrics.screenWidth/2.5, alignItems : 'flex-end', justifyContent : 'flex-end'}}>
                    <Text style={[s.sub_text, {ellipsizeMode : 'tail', color : '#888888'}]}>{props.seat}</Text>
                    </View>}
                    </View>
                </View>

                <View style={[s.card_pax_content,{flex : 1}]}>
                {/* {(props.num_id && props.onPressChangeSeat)  &&  */}
                {/* <Touchable onPress={props.onPressChangeSeat}>
                    <View style={{flex:1, justifyContent : 'flex-end', alignItems : 'flex-end'}}>
                        <View style={s.btn_short_dtl}>
                            <Text style={s.btn_short_dtl_txt}>Pindah Kursi</Text>
                        </View>
                    </View>
                </Touchable> */}
                {/* } */}
                </View>
            </View>
        )
    }
}


export class CardShort extends Component {
    constructor(props) {
        super(props)
        this.state = {}
    }

    render() {
        let { props } = this
        let Touchable = _OS(TouchableWithoutFeedback, TouchableNativeFeedback)


        return (
            <View style={[s.card_pax_frame]}>
                {props.index && <Text style={s.name}>{props.index}.</Text>}
                <View style={s.card_pax_content}>
                    {props.name     && <Text style={s.name}>{props.name}</Text>}
                    </View>
                <View style={[s.card_pax_content,{flex : 1}]}>
                {/* {(props.num_id && props.onPressChangeSeat)  &&  */}
                {/* <Touchable onPress={props.onPressChangeSeat}>
                    <View style={{flex:1, justifyContent : 'flex-end', alignItems : 'flex-end'}}>
                        <View style={s.btn_short_dtl}>
                            <Text style={s.btn_short_dtl_txt}>Pindah Kursi</Text>
                        </View>
                    </View>
                </Touchable> */}
                {/* } */}
                </View>
            </View>
        )
    }
}


export class CardSortTrain extends Component {
    
    constructor(props) {
        super(props);
        this.state = {
            text: 'http://facebook.github.io/react-native/',
        }
    }

    render() {
        let { props } = this
        let {
            train_name,
            onPress,
            onPressDetil,
            title,
            route,
            date,
            nodetail,
            bookCode,
            alldetil
        } = this.props
        let Touchable = _OS(TouchableWithoutFeedback, TouchableNativeFeedback)

        return (
            
            <View style={{ paddingTop:Scale(5),paddingBottom:Scale(5),paddingRight:Scale(15),paddingLeft:Scale(15),flexDirection: 'row', backgroundColor:'#4778fb' }}>
                <View style={[s.card_frame_short, {flex :1,flexDirection:'column'}]}>
                    <View style={{flexDirection:'row', justifyContent: 'space-between',}}>
                        <Text>  </Text>
                        <Text>  </Text>
                        <Text style={{fontSize:Fonts.size.medium, fontWeight:'bold'}}>{date}</Text>
                        <Text>  </Text>
                        <Text>  </Text>
                        <Text>  </Text>
                    </View>
                    <View style={{flexDirection:'row', justifyContent: 'space-between',}}>
                        <View style={s.btn_short_dtl}>
                            <Text style={{color:'white',left:Scale(5)}}>{title==='Kereta Pergi'?'Depart':'return'}</Text>
                        </View>
                        <Text>  </Text>
                        <Text>  </Text>
                        <Text style={{color:'grey'}}>{route}</Text>
                        <Text>  </Text>
                        <Text>  </Text>
                        <Text>  </Text>

                        {nodetail && <Touchable onPress={onPress}>
                            <View>
                                <Text style={s.btn_short_dtl_txt}>&#10140;</Text>
                            </View>
                        </Touchable>}
                    </View>
                </View>

                {bookCode && <Touchable onPress={onPress}>
                    <View style={{ flex: 1, justifyContent: 'flex-end', alignItems: 'center', paddingHorizontal: Scale(16), paddingVertical: Scale(8) }}>
                        <Text style={[s.short_type]}>{'Kode Boking'}</Text>
                        <View style={{ flex: 1, paddingVertical: Metrics.padding.small }}>
                            <QRCode
                                text={bookCode}
                                size={60}
                                bgColor='#000'
                                fgColor='#FFF'
                            />
                        </View>
                        <Text style={[s.short_route, { fontSize: Scale(16), color: Colors.tangerine }]}>{bookCode}</Text>
                    </View></Touchable>}
            </View>
            
        )
    }
}
export class CardSortPelni extends Component {
    
    constructor(props) {
        super(props);
        this.state = {
         
        }
    }
 

    render() {
        let { props } = this
        let {
            train_name,
            onPress,
            title,
            route,
            date,
            image,
            time
        } = this.props
        let Touchable = _OS(TouchableWithoutFeedback, TouchableNativeFeedback)

        return (
            <Touchable onPress={onPress}>
            <View style={{marginHorizontal: Metrics.padding.normal, marginVertical: Metrics.padding.small}}>
                <Text style={s.short_type}>{title}</Text>
                <View style ={{flexDirection :'row'}}>
                    <View style={{paddingVertical: Metrics.padding.small}}>
                        {image && <Image
                            source={image}
                            resizeMode='contain'
                            style={s.imgPelni}
                        />}
                    </View>
                    <View style={[s.card_frame_short_pelni]}>
                        <Text style={s.short_route}>{route}</Text>
                        <Text style={s.short_date}>{date}</Text>
                        <Text style={s.short_date}>{time}</Text>
                    </View>
                </View>
            </View>
            </Touchable>
        )
    }
}


const s = StyleSheet.create({
    sub_text: {
        color: "#222222",
        fontSize: Scale(13)
    },
    card_pax_content:{
        paddingLeft: Scale(8),
    },
    name:{
        fontSize: Scale(14),
        lineHeight: 24,
        letterSpacing: 0,
        color: "#222222"
    },
    card_pax_frame:{

        paddingHorizontal: Scale(16),
        paddingVertical: Scale(8),
        flexDirection: 'row',
    },
    short_type: {
        fontSize: Fonts.size.regular,
        color: Colors.blumine,
        marginBottom: Scale(8),
    },
    short_route: {
        fontSize: Fonts.size.medium,
        lineHeight: Fonts.size.medium * 1.5,
    },
    short_date: {
        fontSize: Fonts.size.small,
        lineHeight: Fonts.size.small * 1.5,
    },
    short_train: {
        fontSize: Fonts.size.small,
        lineHeight: Fonts.size.small * 1.5,
        color: Colors.warm_grey
    },
    btn_short_dtl: {
        backgroundColor: '#4778fb',
        borderRadius: Scale(20),
        height:Scale(20),
        width:Scale(50),
        position: 'absolute',
        left: Scale(10),
        bottom: Scale(10),
        
    },
    dtl_absolute:{
        position: 'absolute',
        right: 0,
        bottom: 0,
        top: 0,
        alignItems: 'center',
        justifyContent: 'center',
        paddingHorizontal: Scale(16)

    },
    btn_short_dtl_txt:{
        color: Colors.pizzaz,
        fontSize:Fonts.size.medium
    },
    btn_short_dtl_txt1:{
        color: 'white'
    },
    card_frame: {
        padding: Metrics.sizePad,
        borderRadius:20
    },
    card_frame_short: {
        padding:Scale(12),
        borderRadius:20,
        backgroundColor:'white'
    },
    card_row: {
        flexDirection: 'row',

    },
    card_left: {
        flex: 1
    },
    card_right: {
        alignItems: 'flex-end',
    },
    card_separator_route: {
        width: Scale(10),
        height: Scale(2),
        alignSelf: 'center',
        backgroundColor: Colors.blumine,
        marginRight: Scale(8),
    },
    card_seats_green: {
        color: Colors.apple,
        fontSize: Fonts.size.small
    },
    card_seats_red:{
        color: Colors.red,
        fontSize: Fonts.size.small
    },
    card_route_time: {
        fontSize: Fonts.size.medium,
    },
    card_route_code: {
        color: Colors.warm_grey,
        fontSize: Fonts.size.tiny
    },
    card_train_name: {
        marginBottom: Scale(4),
    },
    card_routes: {
        marginRight: Scale(8),
        marginBottom: Scale(4),
    },
    card_class: {
        color: Colors.warm_grey,
        fontSize: Fonts.size.small,
    },
    card_stopover: {
        color: Colors.warm_grey
    },
    card_price: {
        alignItems: 'flex-end',
        fontSize: Scale(14),
        color: Colors.pizzaz,
        ...Fonts.bold,
    },
    card_view_detail: {
        color: Colors.blumine
    }
})
