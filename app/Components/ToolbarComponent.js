import React, { Component } from 'react'
import { Image, TouchableNativeFeedback, TouchableWithoutFeedback, View, StyleSheet, Platform } from 'react-native'
import { Colors, Metrics, getIcon, _OS } from '../Assets'

export class Toolbar extends Component {
    render() {
        const {
            arrow_back,
        } = this.props
        const TouchableComponent = Platform.OS === 'ios' ? TouchableWithoutFeedback : TouchableNativeFeedback
        return (
            <View style={[styles.toolbar_frame, this.props.style]}>
                {arrow_back &&
                    <TouchableComponent
                        onPress={this.props.onPress} >
                        <Image
                            style={[styles.icon]}
                            resizeMode='contain'
                            source={getIcon('ic_arrow_back')}
                        />
                    </TouchableComponent>}
                {this.props.View || this.props.children}
                {this.props.Icon}
            </View>
        )
    }
}


export class ToolbarCheckout extends Component {
    render() {
        const {
            arrow_back,
        } = this.props
        const TouchableComponent = Platform.OS === 'ios' ? TouchableWithoutFeedback : TouchableNativeFeedback
        return (
            <View style={[styles.toolbar_frame_checkout, this.props.style]}>
                {arrow_back &&
                    <TouchableComponent
                        onPress={this.props.onPress} >
                        <Image
                            style={[styles.icon]}
                            resizeMode='contain'
                            source={getIcon('ic_arrow_back')}
                        />
                    </TouchableComponent>}
                {this.props.View || this.props.children}
                {this.props.Icon}
            </View>
        )
    }
}


export class ToolbarHome extends Component {
    render() {
        const {
            arrow_back,
        } = this.props
        const TouchableComponent = Platform.OS === 'ios' ? TouchableWithoutFeedback : TouchableNativeFeedback
        return (
            <View style={[styles.toolbar_frame_home, this.props.style]}>
                {arrow_back &&
                    <TouchableComponent
                        onPress={this.props.onPress} >
                        <Image
                            style={[styles.icon]}
                            resizeMode='contain'
                            source={getIcon('ic_arrow_back')}
                        />
                    </TouchableComponent>}
                {this.props.View || this.props.children}
                {this.props.Icon}
            </View>
        )
    }
}

export class ToolbarTrain extends Component {
    render() {
        const {
            arrow_back,
        } = this.props
        const TouchableComponent = Platform.OS === 'ios' ? TouchableWithoutFeedback : TouchableNativeFeedback
        return (
            <View style={[styles.toolbar_frame_train, this.props.style]}>
                {arrow_back &&
                    <TouchableComponent
                        onPress={this.props.onPress} >
                        <Image
                            style={[styles.icon]}
                            resizeMode='contain'
                            source={getIcon('ic_arrow_back')}
                        />
                    </TouchableComponent>}
                {this.props.View || this.props.children}
                {this.props.Icon}
            </View>
        )
    }
}
var styles = StyleSheet.create({
    toolbar_frame_home: {
        flexDirection: 'row',
        alignItems: 'center',
        paddingLeft: 0,
        paddingRight: 0,
        paddingTop: _OS(20, 0),
        height: 170 + _OS(20, 0),
        backgroundColor: 'white'
    },
    toolbar_frame_train: {
        flexDirection: 'row',
        alignItems: 'center',
        paddingLeft: 0,
        paddingRight: 0,
        paddingTop: _OS(20, 0),
        height: 390 + _OS(20, 0),
        backgroundColor: 'white'
    },
    toolbar_frame: {
        flexDirection: 'row',
        alignItems: 'center',
        paddingLeft: Metrics.normalPadding,
        paddingRight: Metrics.normalPadding,
        paddingTop: _OS(20, 0),
        height: 56.6 + _OS(20, 0),
        backgroundColor:'#4778fb'
    },
    toolbar_frame_checkout: {
        flexDirection: 'row',
        alignItems: 'center',
        paddingLeft: Metrics.normalPadding,
        paddingRight: Metrics.normalPadding,
        paddingTop: _OS(20, 0),
        height: 200 + _OS(20, 0),
        backgroundColor: Colors.blumine
    },
    icon: {
        marginRight: Metrics.normalPadding,
        width: 24,
        height: 24,
    }
})