import React, { Component } from 'react'
import {
    View,
    Alert as Confirmation,
    StyleSheet,
    TouchableWithoutFeedback,
    TouchableOpacity,
    TouchableNativeFeedback, Image
} from 'react-native'
import {
    Scale,
    Colors,
    getIcon,
    Metrics,
    Fonts,
} from '../Assets'
import { Function, navigateTo } from '../Utils'
import { Icon } from './IconComponent'
import Text from './TextView'
import { _OS } from './_OS';
import QRCode from 'react-native-qrcode'
import Modal from 'react-native-modal';
import { NavigationActions } from 'react-navigation';
export class CardPulsa extends Component {
    constructor(props) {
        super(props);
        this.state = {
            visibleModal: null,
        }
    }
    Confirmation() {
        let {
            description,
            operator,          
            amount,           
            rsPrice,
            status,
            nomor
        } = this.props
        Confirmation.alert(
            'Pesanan Anda sudah benar ?',
            'Anda tidak akan bisa mengubah detail pesanan setelah melanjutkan kehalaman pembayaran. Tetap lanjutkan ?',
            [
              {text: 'Periksa Kembali', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
              {text: 'Ya, Lanjutkan', onPress: () => this.Validate()},
            ],
            { cancelable: false }
          )
    }

    Validate(){
        let {
            description,
            operator,
            amount,
            rsPrice,
            status,
            nomor,
            Goto
        } = this.props
        // const { navigation } = this.props;
        if (nomor!==null){
            const navigateAction = NavigationActions.navigate({
                routeName: 'PulsaPayment',
                Params:{
                 description:description, 
                 operator:operator, 
                 amount:amount, 
                 rsPrice: rsPrice,
                 status:status, nomor 
             } });
              this.props.navigation.dispatch(navigateAction);
        }else{
            Confirmation.alert("Isi Nomor Dulu yaaaaaa!!!!!!!")
        }


    }

    render() {
        let {
            description,
            operator,          
            amount,           
            rsPrice,
            status,
        } = this.props
        let Touchable = _OS(TouchableWithoutFeedback, TouchableNativeFeedback)

        return (
            <Touchable onPress={()=> this.Confirmation()}>
           
                <View style={[s.card_frame, {backgroundColor: Colors.whitesmoke}]}>
                
                    <View style={[s.card_row]}>
                        <View style={[s.card_left]}>
                            <Text style={[s.card_train_name]}>
                                <Text>Voucher Rp. {Function.convertToPrice(amount)} </Text>
                            </Text>
                            <View style={[s.card_row]}>
                                <View style={[s.card_routes]}>
                                    {operator && <Text style={s.card_route_time}>{operator}</Text>}
                                </View>
                            </View>
                        </View>
                        <View style={[s.card_right]}>
                            {rsPrice && <Text style={[s.card_price]}>Rp. {Function.convertToPrice(rsPrice)}</Text>}
                            {amount && <Text style={[s.card_class]}>Rp. {Function.convertToPrice(amount)}</Text>}
                        </View>
                    </View>
                    
                </View>
            </Touchable>
        )
    }
}

const s = StyleSheet.create({
    sub_text: {
        color: "#222222",
        fontSize: Scale(13)
    },
    card_pax_content:{
        paddingLeft: Scale(8),
    },
    name:{
        fontSize: Scale(14),
        lineHeight: 24,
        letterSpacing: 0,
        color: "#222222"
    },
    card_pax_frame:{

        paddingHorizontal: Scale(16),
        paddingVertical: Scale(8),
        flexDirection: 'row',
    },
    short_type: {
        fontSize: Fonts.size.regular,
        color: Colors.blumine,
        marginBottom: Scale(8),
    },
    short_route: {
        fontSize: Fonts.size.medium,
        lineHeight: Fonts.size.medium * 1.5,
    },
    short_date: {
        fontSize: Fonts.size.small,
        lineHeight: Fonts.size.small * 1.5,
    },
    short_train: {
        fontSize: Fonts.size.small,
        lineHeight: Fonts.size.small * 1.5,
        color: Colors.warm_grey
    },
    btn_short_dtl: {
        backgroundColor: Colors.transparent,
        borderColor: Colors.pizzaz,
        borderWidth: Scale(1),
        paddingHorizontal: Scale(8),
        paddingVertical: Scale(2),
        borderRadius: Scale(3),
    },
    dtl_absolute:{
        position: 'absolute',
        right: 0,
        bottom: 0,
        top: 0,
        alignItems: 'center',
        justifyContent: 'center',
        paddingHorizontal: Scale(16)

    },
    btn_short_dtl_txt:{
        color: Colors.pizzaz
    },
    card_frame: {
        padding: Metrics.sizePad
    },
    card_frame_short: {
        paddingHorizontal:Scale(16),
        paddingVertical: Scale(8),
    },
    card_row: {
        flexDirection: 'row',

    },
    card_left: {
        flex: 1
    },
    card_right: {
        alignItems: 'flex-end',
    },
    card_separator_route: {
        width: Scale(10),
        height: Scale(2),
        alignSelf: 'center',
        backgroundColor: Colors.blumine,
        marginRight: Scale(8),
    },
    card_seats_green: {
        color: Colors.apple,
        fontSize: Fonts.size.small
    },
    card_seats_red:{
        color: Colors.red,
        fontSize: Fonts.size.small
    },
    card_route_time: {
        fontSize: Fonts.size.medium,
    },
    card_route_code: {
        color: Colors.warm_grey,
        fontSize: Fonts.size.tiny
    },
    card_train_name: {
        marginBottom: Scale(4),
    },
    card_routes: {
        marginRight: Scale(8),
        marginBottom: Scale(4),
    },
    card_class: {
        color: Colors.warm_grey,
        fontSize: Fonts.size.small,
    },
    card_stopover: {
        color: Colors.warm_grey
    },
    card_price: {
        alignItems: 'flex-end',
        fontSize: Scale(14),
        color: Colors.pizzaz,
        ...Fonts.bold,
        textDecorationLine: 'line-through'
    },
    card_view_detail: {
        color: Colors.blumine
    },
    button: {
        backgroundColor: 'lightblue',
        padding: 12,
        margin: 16,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 4,
        borderColor: 'rgba(0, 0, 0, 0.1)',
      },
      modalContent: {
        backgroundColor: 'white',
        padding: 22,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 4,
        borderColor: 'rgba(0, 0, 0, 0.1)',
      },
      bottomModal: {
        justifyContent: 'flex-end',
        margin: 0,
      },
})
